//Librerias
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
using namespace std;

#include "ArbolAVL.h"

// Menu
int menu(){
    string opc;

    cout << "\n";
    cout << "---------------------------" << endl;
    cout << "Insertar ID             [1]" << endl;
    cout << "Eliminar ID             [2]" << endl;
    cout << "Modificar ID            [3]" << endl;
    cout << "Generar grafo           [4]" << endl;
    cout << "Salir                   [0]" << endl;
    cout << "---------------------------" << endl;
    cout << "Opcion: ";
    cin >> opc;

  return stoi(opc);
}

// Funcion principal del programa
int main(void) {

    // Creacion de Objetos
    Nodo *arbol = NULL;
    ArbolAVL *arbol_avl = new ArbolAVL();

    //Variables
    int opc = 0;
    string line;
    
    ifstream archivo; // leer
    string codigo_pdb;
    bool BO = false;
    archivo.open("pdb.txt");

    // EN caso de no encontrar el archivo
    if (archivo.fail()) {
        cout << "Error al abrir el archivo." << endl;
        exit(1);
    }

    // Archivo abierto
    while (!archivo.eof()) {
        getline(archivo, codigo_pdb);
        //arbol_avl->insertar(arbol, BO, codigo_pdb);
    }
    arbol_avl->crear_grafo(arbol);

    // INteraccion con el menu
    system("clear");
    do {
            opc = menu();

            switch (opc) {
                // Insercion
                case 1:
                    cout << "Ingresar elemento: ";
                    cin >> line;
                    arbol_avl->InsercionBalanceado(arbol, BO, line);
                    arbol_avl->crear_grafo(arbol);
                    break;

                // ELiminacion
                case 2:
                    cout << "Eliminar elemento: ";
                    cin >> line;
                    arbol_avl->EliminacionBalanceado(arbol, BO, line);
                    arbol_avl->crear_grafo(arbol);
                    break;

                // MOdificacion
                case 3:     
                    cout << "Ingrese ID que desee modificar: ";
                    cin >> line;
                    if(arbol_avl->Busqueda(arbol,line) == true){
                        //cout << "Elemento " << line << " encontrado" << endl;
                        arbol_avl->EliminacionBalanceado(arbol, BO, line);
                        cout << endl;
                        cout << "Ingrese ID por el que desea modificar: ";
                        cin >> line;
                        arbol_avl->InsercionBalanceado(arbol, BO, line);
                    }
                    else{
                        cout << "Elemento no encontrado ";
                    }
                    cout << endl;           
                    break;

                // Genera el grafo/ mostrar arbol
                case 4:
                    arbol_avl->crear_grafo(arbol);
                    break;
                    
            }
        } while (opc != 0); // Sale del programa
        
    return 0;
}
