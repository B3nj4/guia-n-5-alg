#ifndef ARBOLAVL_H
#define ARBOLAVL_H

//Librerias
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
using namespace std;

// Clase de nodo 
struct Nodo {
    string codigo_pdb;
    int FE;
    struct Nodo *derecha;
    struct Nodo *izquierda;
};


// CLase del arbol
class ArbolAVL {
    private:
        ofstream archivo; // salida
        //bool BO = false;

    public:
        //Constructor
        ArbolAVL();
        
        // Insercion
        void InsercionBalanceado(Nodo *&arbol, bool &BO, string infor);

        // Eliminacion
        void EliminacionBalanceado(Nodo *arbol, bool &BO, string infor);
        void Restructura1(Nodo *arbol, bool &BO);
        void Restructura2(Nodo *arbol, bool &BO);
        void Borra(Nodo *aux1, Nodo *otro1, bool &BO);

        // Modificacion
        bool Busqueda(Nodo *arbol, string infor);

        // Grafo
        void recorrer_arbol(Nodo *arbol);
        void crear_grafo(Nodo *arbol);
};
#endif