#include <fstream>
#include <iostream>
using namespace std;

//Librerias
#include "ArbolAVL.h"

ArbolAVL::ArbolAVL() {}

// Insercion
void ArbolAVL::InsercionBalanceado(Nodo *&arbol, bool &BO, string infor){
    //instancian nodos auxiliares
    Nodo *nodo = NULL;
    Nodo *nodo1 = NULL;
    Nodo *nodo2 = NULL;

    // se iguala un nodo auxiliar al nodo ingresado en la funcion
    nodo = arbol;

    if (nodo != NULL){
        if(infor < nodo->codigo_pdb){
            InsercionBalanceado(nodo->izquierda, BO, infor);
        }

        if(BO == true){
            switch (nodo->FE) {
                case 1: 
                    nodo->FE = 0;
                    BO = true;
                    break;
                
                case 0: 
                    nodo->FE = -1;
                    break;
            
                case -1: 
                    /* reestructuración del árbol */
                    nodo1 = nodo->izquierda;
                    
                    /* Rotacion II */
                    if (nodo1->FE <= 0) { 
                        nodo->izquierda = nodo1->derecha;
                        nodo1->derecha = nodo;
                        nodo->FE = 0;
                        nodo = nodo1;
                    } 
                    else { 
                        /* Rotacion ID */
                        nodo2 = nodo1->derecha;
                        nodo->izquierda = nodo2->derecha;
                        nodo2->derecha = nodo;
                        nodo1->derecha = nodo2->izquierda;
                        nodo2->izquierda = nodo1;
                    
                        if (nodo2->FE == -1)
                            nodo->FE = 1;
                        else
                            nodo->FE =0;
                    
                        if (nodo2->FE == 1)
                            nodo1->FE = -1;
                        else
                            nodo1->FE = 0;
                        
                        nodo = nodo2;
                    }
                    
                    nodo->FE = 0;
                    BO = false;
                    break;
            }  
        }
    
        else {
        
            if (infor > nodo->codigo_pdb) {
                InsercionBalanceado(nodo->derecha, BO, infor);
                
                if (BO == true) {
                
                    switch (nodo->FE) {
                        
                        case -1: 
                            nodo->FE = 0;
                            BO = false;
                            break;
                        
                        case 0: 
                            nodo->FE = 1;
                            break;
                
                        case 1: 
                            /* reestructuración del árbol */
                            nodo1 = nodo->derecha;
                            
                            if (nodo1->FE >= 0) { 
                                /* Rotacion DD */
                                nodo->derecha = nodo1->izquierda;
                                nodo1->izquierda = nodo;
                                nodo->FE = 0;
                                nodo = nodo1;
                                
                            } else { 
                                /* Rotacion DI */
                                nodo2 = nodo1->izquierda;
                                nodo->derecha = nodo2->izquierda;
                                nodo2->izquierda = nodo;
                                nodo1->izquierda = nodo2->derecha;
                                nodo2->derecha = nodo1;
                                
                                if (nodo2->FE == 1)
                                nodo->FE = -1;
                                else
                                nodo->FE = 0;
                    
                                if (nodo2->FE == -1)
                                nodo1->FE = 1;
                                else
                                nodo1->FE = 0;
                
                                nodo = nodo2;
                            }
                            
                            nodo->FE = 0;
                            BO = false;
                            break;
                    }
                }
            } 
            
            else {
                cout << "El nodo ya se encuentra en el árbol" << endl;
            }
       
        }
    
    }

    else {
        //nodo = (struct _Nodo) malloc (sizeof(Nodo));
        nodo->izquierda = NULL;
        nodo->derecha = NULL;
        nodo->codigo_pdb = infor;
        nodo->FE = 0;
        BO = true;
    }
    
    arbol = nodo;
}


// FUncion para recorrer el arbol 
void ArbolAVL::recorrer_arbol(Nodo *arbol) {
    if(arbol != NULL){
        archivo << "\n" << '"' << arbol->codigo_pdb << '"' << "->" << '"' << arbol->codigo_pdb << '"' << "[label=" << arbol->FE << "]" << ";";
        if(arbol->izquierda != NULL){
            archivo << "\n" << '"' << arbol->codigo_pdb << '"' << "->" << '"' << arbol->izquierda->codigo_pdb << '"' << ";";
        }
        else{
            archivo << "\n" << '"' << arbol->codigo_pdb << "i" << '"' << " [shape=point];"; 
            archivo << "\n" << '"' << arbol->codigo_pdb << '"' << "->" << '"' << arbol->codigo_pdb << "i" << '"';
        }
        if(arbol->derecha != NULL){
            archivo << "\n" << '"' << arbol->codigo_pdb << '"' << "->" << '"' << arbol->derecha->codigo_pdb << '"' << ";";
        }
        else{
            archivo << "\n" << '"' << arbol->codigo_pdb << "d" << '"' << " [shape=point];";
            archivo << "\n" << '"' << arbol->codigo_pdb << '"' << "->" << '"' << arbol->codigo_pdb << "d" << '"';
        }
        //Evaluar por ambos lados del arbol
        recorrer_arbol(arbol->izquierda);
        recorrer_arbol(arbol->derecha);
    }
}


// Creacion del grafo en un png
void ArbolAVL::crear_grafo(Nodo *arbol) {
    //Apertura del grafo
    archivo.open("grafo.txt");
    
    // Evaluar la existencia del archivo grafo
    if(archivo.fail()){
        cout << "No se puedo abrir el archivo";
        exit(1);
    }
    archivo << "digraph G {" << endl;
    archivo << "node [style=filled fillcolor=yellow];";
    recorrer_arbol(arbol);
    archivo << "}";
    
    //Cerrar el archivo grafo
    archivo.close();
    
    // Impresion de los archivos .txt y .png
    system("dot -Tpng -ografo.png grafo.txt &");
    system("eog grafo.png &");
}


//REestructuracion para eliminacion 
void ArbolAVL::Restructura1(Nodo *arbol, bool &BO) {
    Nodo *nodo;
    Nodo *nodo1;
    Nodo *nodo2; 
    nodo = arbol;
    
    if (BO == true) {
        switch (nodo->FE) {
        case -1: 
            nodo->FE = 0;
            break;
        
        case 0: 
            nodo->FE = 1;
            BO = false;
            break;
    
        case 1: 
            /* reestructuracion del árbol */
            nodo1 = nodo->derecha;
            
            if (nodo1->FE >= 0) { 
                /* rotacion DD */
                nodo->derecha = nodo1->izquierda;
                nodo1->izquierda = nodo;
                
                switch (nodo1->FE) {
                    case 0: 
                        nodo->FE = 1;
                        nodo1->FE = -1;
                        BO = false;
                        break;
                    case 1: 
                        nodo->FE = 0;
                        nodo1->FE = 0;
                        BO = false;
                        break;           
                }
                nodo = nodo1;
            } 
            else { 
                /* Rotacion DI */
                nodo2 = nodo1->izquierda;
                nodo->derecha = nodo2->izquierda;
                nodo2->izquierda = nodo;
                nodo1->izquierda = nodo2->derecha;
                nodo2->derecha = nodo1;
            
                if (nodo2->FE == 1)
                nodo->FE = -1;
                else
                nodo->FE = 0;
                
                if (nodo2->FE == -1)
                nodo1->FE = 1;
                else
                nodo1->FE = 0;
            
                nodo = nodo2;
                nodo2->FE = 0;       
            } 
        break;   
        }
    }
    arbol = nodo;
}

void ArbolAVL::Restructura2(Nodo *arbol, bool &BO) {
    Nodo *nodo;
    Nodo *nodo1;
    Nodo *nodo2; 
    nodo = arbol;
    
    if (BO == true) {
        switch (nodo->FE) {
            case 1: 
                nodo->FE = 0;
                break;
            case 0: 
                nodo->FE = -1;
                BO = false;
                break;
            case -1: 
                /* reestructuracion del árbol */
                nodo1 = nodo->izquierda;
                if (nodo1->FE<=0) { 
                    /* rotacion II */
                    nodo->izquierda = nodo1->derecha;
                    nodo1->derecha = nodo;
                    switch (nodo1->FE) {
                        case 0: 
                            nodo->FE = -1;
                            nodo1->FE = 1;
                            BO = false;
                            break;
                        case -1: 
                            nodo->FE = 0;
                            nodo1->FE = 0;
                            BO = false;
                            break;
                    }
                    nodo = nodo1;
                } 
                else { 
                    /* Rotacion ID */
                    nodo2 = nodo1->derecha;
                    nodo->izquierda = nodo2->derecha;
                    nodo2->derecha = nodo;
                    nodo1->derecha = nodo2->izquierda;
                    nodo2->izquierda = nodo1;
                
                    if (nodo2->FE == -1)
                        nodo->FE = 1;
                    else
                        nodo->FE = 0;
                    
                    if (nodo2->FE == 1)
                        nodo1->FE = -1;
                    else
                        nodo1->FE = 0;
                
                    nodo = nodo2;
                    nodo2->FE = 0;       
                }      
                break;   
        }
    }
    arbol = nodo;
}

// Delete
void ArbolAVL::Borra(Nodo *aux1, Nodo *otro1, bool &BO) {
    // Instanciacion
    Nodo *aux;
    Nodo *otro;

    aux = aux1;
    otro = otro1;
    
    if (aux->derecha != NULL) {
        Borra(aux->derecha,otro,BO); 
        Restructura2(aux,BO);
    } 
    else {
        otro->codigo_pdb = aux->codigo_pdb;
        aux = aux->izquierda;
        BO = true;
    }
    
    aux1=aux;
    otro1=otro;
}

// ELiminacion
void ArbolAVL::EliminacionBalanceado(Nodo *arbol, bool &BO, string infor) {
    //INstancia de nodos auxiliares
    Nodo *nodo;
    Nodo *otro;
    
    // NOdo auxiliar se iguala al arbol ingresado
    nodo = arbol;
  
    if (nodo != NULL) {
        if (infor < nodo->codigo_pdb) {
            EliminacionBalanceado(nodo->izquierda,BO,infor);
            Restructura1(nodo,BO);
        } 
        else {
            if (infor > nodo->codigo_pdb) {
                EliminacionBalanceado(nodo->derecha,BO,infor);
                Restructura2(nodo,BO); 
            } 
            else {
                otro = nodo;
                if (otro->derecha == NULL) {
                    nodo = otro->izquierda;
                    BO = true;     
                } 
                else {
                    if (otro->izquierda == NULL) {
                        nodo = otro->derecha;
                        BO = true;     
                    } 
                    else {
                        Borra(otro->izquierda,otro,BO);
                        Restructura1(nodo,BO);
                        free(otro);
                    }
                }
            }
        } 
    } 
    
    else {
        cout << "El nodo NO se encuentra en el árbol" << endl;
    }
    
    arbol = nodo;
}


bool ArbolAVL::Busqueda(Nodo *arbol, string infor) {
    // Si el arbol esta vacio no hay elementos true
    if(arbol == NULL){
        return false;
    }
    // si se encuentra el numero n, retorna true
    else if(arbol->codigo_pdb == infor){
        return true;
    }
    // buscar por la der
    else if(infor < arbol->codigo_pdb){
        return Busqueda(arbol->izquierda, infor);
    }
    // buscar por la izq
    else{
        return Busqueda(arbol->derecha, infor);
    }
    
    /*
    if (arbol != NULL) {
        if (infor == nodo->info) {
            Busqueda(arbol->izquierda,infor);
        } 
        else {
            if (infor > nodo->info) {
                Busqueda(arbol->derecha,infor);
            } 
            else {
                cout << "El nodo SI se encuentra en el árbol" << endl;
            }
        }
    } 
    else {
        cout << "El nodo NO se encuentra en el árbol" << endl;
    } */
}